function MyArray(arg, ...args) {
  let { length } = arguments;

  if (!(this instanceof MyArray)) {
    return new MyArray(arg, ...args);
  }

  if (arguments.length === 1 && (arg < 0 || arg > 2 ** 32 - 1 || Number.isNaN(arg))) {
    throw new RangeError('Invalid array length');
  }

  if (length === 1 && isNaN(arg)) {
    this[0] = arg;
    this.length = 1;
    return this;
  }

  if (arguments.length === 1 && typeof arg === 'number') {
    this.length = arg;
    return this;
  }

  for (let i = 0; i < length; i++) {
    this[i] = arguments[i];
  }

  Object.defineProperty(this, 'length', {
    enumerable: false,
    set(newLength) {
      if (typeof newLength !== 'number') {
        throw new RangeError('Invalid array length');
      }

      if (length > newLength) {
        for (let i = length - 1; i >= newLength; i--) {
          delete this[i];
        }
      }
      length = newLength;
    },
    get() {
      return length;
    }
  });

  return this;
}
MyArray.prototype.push = function() {
  if (typeof this.length !== 'number') {
    this.length = 0;
  }

  for (let i = 0; i < arguments.length; i++) {
    this[this.length] = arguments[i];
    this.length += 1;
  }
  return this.length;
};

Object.defineProperty(MyArray.prototype.push, 'length', { value: 1, writable: false });

MyArray.prototype.pop = function() {
  if (this.length === 0) {
    return;
  }

  if (!this.length) {
    this.length = 0;
    return this.length;
  }

  const deletedElement = this[this.length - 1];
  delete this[this.length - 1];
  this.length -= 1;
  return deletedElement;
};

MyArray.from = function(obj, ...args) {
  const newarr = new MyArray();

  for (let i = 0; i < obj.length; i++) {
    newarr.push(obj[i]);

    if (args[0]) {
      newarr[i] = args[0].call(args[1], obj[i], i, obj);
    }
  }
  return newarr;
};

MyArray.prototype.forEach = function(func, context = this) {
  const tempFunc = new MyArray(this.length);
  const callback = func.bind(context);

  for (let i = 0; i < tempFunc.length; i++) {
    if (i in this) {
      callback(this[i], i, this);
    }
  }
};

MyArray.prototype.map = function(func, context = this) {
  const thisArgCallBack = func.bind(context);
  const tempArray = new MyArray(this.length);

  for (let i = 0; i < tempArray.length; i++) {
    if (this[i] === undefined) {
      i += 1;
    }

    if (i in this) {
      tempArray[i] = thisArgCallBack(this[i], i, this);
    }
  }

  return tempArray;
};

MyArray.prototype.reduce = function(func) {
  const beginIndex = arguments.length > 1 ? 0 : 1;
  let accumulator = arguments.length > 1 ? arguments[1] : this[0];
  const curLength = this.length;

  if (!curLength && arguments.length === 1) {
    throw new TypeError();
  }

  for (let i = beginIndex; i < curLength; i++) {
    if (i in this) {
      accumulator = func(accumulator, this[i], i, this);
    }
  }

  return accumulator;
};


MyArray.prototype.filter = function(func, context = this) {
  const tempFunct = func.bind(context);
  const newArr = new MyArray(this.length);
  let count = 0;

  for (let i = 0; i < newArr.length; i++) {
    if (this[i]) {
      if (tempFunct(this[i], i, this)) {
        newArr[i] = this[i];
        count += 1;
      }
    }
  }
  newArr.length = count;
  return newArr;
};

MyArray.prototype.sort = function(func) {
  const lengthArr = this.length;

  if (!func) {
    for (let i = 0; i < lengthArr; i++) {
      for (let k = 0; k < lengthArr - 1; k++) {
        if (String(this[k + 1]) < (String(this[k]))) {
          const temp = this[k + 1];
          this[k + 1] = this[k];
          this[k] = temp;
        }
      }
    }

    return this;
  }

  const tempArr = new MyArray();

  for (let i = 1; i < lengthArr; i++) {
    if (this[i]) {
      tempArr.push(func(this[i], this[i - 1]));
    }
  }

  let returnValuesEqual = null;

  for (let i = 0; i < tempArr.length; i++) {
    returnValuesEqual = tempArr[i] === -1 ? 'yes' : 'no';
  }

  if (returnValuesEqual === 'yes') {
    for (let i = 0, j = lengthArr - 1; i < j; i += 1, j -= 1) {
      [this[i], this[j]] = [this[j], this[i]];
    }

    return this;
  }

  for (let i = 0; i < lengthArr; i++) {
    if (tempArr[i] > 0) {
      for (let k = 0; k < lengthArr - 1 - i; k++) {
        if (this[k + 1] < this[k]) {
          const temp = this[k + 1];
          this[k + 1] = this[k];
          this[k] = temp;
        }
      }
    }

    if (tempArr[i] < 0) {
      for (let i = lengthArr - 1; i >= 0; i--) {
        for (let k = lengthArr - 1; k >= 0; k--) {
          if (this[k] > this[k - 1]) {
            const temp = this[k];
            this[k] = this[k - 1];
            this[k - 1] = temp;
          }
        }
      }
    }
  }

  return this;
};

MyArray.prototype.toString = function() {
  let string = '';

  for (let i = 0; i < this.length; i++) {
    string += `${this[i] || ''},`;
  }
  return string.slice(0, -1);
};

MyArray.prototype[Symbol.iterator] = function() {
  let currentIndex = 0;
  const endIndex = this.length;

  return {
    next: () => {
      if (currentIndex < endIndex) {
        const obj = {
          done: false,
          value: this[currentIndex]
        };
        currentIndex += 1;
        return obj;
      }
      else {
        return {
          done: true
        };
      }
    }
  };
};


MyArray.prototype.indexOf = function(searchElement, ...from) {
  if (from[0] < 0) {
    return -1;
  }

  const fromIndex = from[0] ? from[0] : 0;

  for (let i = fromIndex; i < this.length; i++) {
    if (searchElement === this[i]) {
      return i;
    }
  }
  return -1;
};

MyArray.prototype.slice = function(begin, end) {
  const tempArray = new MyArray();
  const copyOfMyArray = Object.assign({}, this);
  const beginIndex = begin ? begin : 0;
  let endIndex = end ? end : this.length;
  let counter = 0;

  if (isNaN(Number(beginIndex))) {
    return copyOfMyArray;
  }

  if (beginIndex === -this.length) {
    return copyOfMyArray;
  }

  if (beginIndex > this.length) {
    return tempArray;
  }

  if (endIndex > this.length) {
    endIndex = this.length;
  }

  if (endIndex < 0) {
    for (let i = beginIndex; i < this.length + endIndex; i++) {
      tempArray[counter] = this[i];
      tempArray.length += 1;
      counter += 1;
    }
    return tempArray;
  }

  if (beginIndex < 0) {
    for (let i = this.length + beginIndex; i < endIndex; i++) {
      tempArray[counter] = this[i];
      tempArray.length += 1;
      counter += 1;
    }
    return tempArray;
  }

  for (let i = beginIndex; i < endIndex; i++) {
    tempArray[counter] = this[i];
    tempArray.length += 1;
    counter += 1;
  }
  return tempArray;
};

MyArray.prototype.splice = function(start, deleteCount, ...args) {
  const tempArray = new MyArray();
  let beginIndex = Math.floor(start);
  let endIndex = deleteCount;
  let counter = 0;


  if (endIndex === undefined) {
    endIndex = this.length;
  }

  if (endIndex >= (this.length - beginIndex)) {
    endIndex = this.length - beginIndex;
  }

  if (beginIndex < 0) {
    beginIndex += this.length;
  }

  if (isNaN(beginIndex) && isNaN(endIndex)) {
    return { ...this };
  }

  if (isNaN(beginIndex)) {
    beginIndex = 0;
    endIndex = this.length;
  }

  if (beginIndex === -Infinity) {
    beginIndex = 0;
  }

  const sumLength = beginIndex + endIndex;

  if (args.length === 0) {
    for (let i = beginIndex; i < sumLength; i++) {
      tempArray[counter] = this[i];
      this[i] = this[i + endIndex];
      counter += 1;
    }
    this.length -= endIndex;
  }

  if (args.length > 0 && endIndex === 0) {
    for (let i = beginIndex; i < args.length + 1; i++) {
      this[i + 1] = this[i];
      this[i] = args[counter];
      counter += 1;
      this.length += 1;
    }
    this.length -= endIndex;
  }

  if (args.length > 0) {
    for (let i = beginIndex; i < sumLength; i++) {
      tempArray[counter] = this[i];
      this[i] = args[counter];
      counter += 1;
    }
    this.length -= endIndex;
  }

  tempArray.length = counter;

  return tempArray;
};


module.exports = MyArray;